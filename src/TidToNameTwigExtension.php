<?php

namespace Drupal\tid_to_name;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides a Twig function to get term name by TID.
 *
 * @package Drupal\tid_to_name
 */
class TidToNameTwigExtension extends AbstractExtension {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TidToNameTwigExtension.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LanguageManagerInterface $language_manager, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager) {
    $this->languageManager = $language_manager;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('tn', $this->getTermNameByTid(...)),
    ];
  }

  /**
   * Returns the name of a term given its TID. Example: {{ tn(123) }}.
   *
   * @param int|string $tid
   *   The term ID.
   *
   * @return string
   *   The term name.
   */
  public function getTermNameByTid(int|string $tid): string {
    // Ensure that TID is a valid integer.
    if (!is_numeric($tid) || intval($tid) <= 0) {
      return '';
    }

    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $term_storage->load(intval($tid));
    if ($term) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      $translated_term = $this->entityRepository->getTranslationFromContext($term, $language);
      if ($translated_term) {
        return $translated_term->getName();
      }
    }

    return '';
  }

}
