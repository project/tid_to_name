<?php

namespace Drupal\Tests\tid_to_name\Unit;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\tid_to_name\TidToNameTwigExtension;
use PHPUnit\Framework\TestCase;

/**
 * Tests the TidToNameTwigExtension class.
 *
 * @group tid_to_name
 */
class TidToNameTwigExtensionTest extends TestCase {

  /**
   * The mocked language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $languageManager;

  /**
   * The mocked entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityRepository;

  /**
   * The mocked entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked language.
   *
   * @var \Drupal\Core\Language\LanguageInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $mockedLanguage;

  /**
   * The mocked term entity.
   *
   * @var \Drupal\taxonomy\TermInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $mockedTerm;

  /**
   * The TidToNameTwigExtension instance.
   *
   * @var \Drupal\tid_to_name\TidToNameTwigExtension
   */
  protected $tidToNameTwigExtension;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Instantiate $this->mockedTerm here before any other operations.
    $this->mockedTerm = $this->createMock(TermInterface::class);

    // Create a mock of LanguageManagerInterface.
    $this->languageManager = $this->createMock(LanguageManagerInterface::class);

    // Create a mock of LanguageInterface.
    $this->mockedLanguage = $this->createMock(LanguageInterface::class);

    // Assuming the languageManager has a method getCurrentLanguage that
    // returns a language.
    $this->languageManager->expects($this->any())
      ->method('getCurrentLanguage')
      ->willReturn($this->mockedLanguage);

    // Assuming the language has a method getId that returns a language code.
    $this->mockedLanguage->expects($this->any())
      ->method('getId')
      ->willReturn('en');

    // Adjust the mocked term to return our mocked language when the language()
    // method is called.
    $this->mockedTerm->expects($this->any())
      ->method('language')
      ->willReturn($this->mockedLanguage);

    // Create a mock of EntityRepositoryInterface and use the returnCallback.
    $this->entityRepository = $this->createMock(EntityRepositoryInterface::class);
    $this->entityRepository->method('getTranslationFromContext')
      ->will($this->returnCallback(
        function ($term) {
          return $term;
        }
      ));

    // Create a mock of EntityTypeManagerInterface.
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);

    // Create the actual object to test, injecting our mocks.
    $this->tidToNameTwigExtension = new TidToNameTwigExtension(
      $this->languageManager,
      $this->entityRepository,
      $this->entityTypeManager
    );

  }

  /**
   * Provides data for the testGetTermNameByTid.
   *
   * @return array
   *   An array of test parameters.
   */
  public static function termNameProvider(): array {
    return [
      // Term ID, Expected Name, Term Exists, Term Name, Language Code.
      [123, 'Sample Term Name', TRUE, 'Sample Term Name', 'en'],
      [123, 'Exemple de nom de terme', TRUE, 'Exemple de nom de terme', 'fr'],
      [-1, '', FALSE, NULL, 'en'],
      ['abc', '', FALSE, NULL, 'en'],
      [999, '', FALSE, NULL, 'en'],
      [125, '0', TRUE, '0', 'en'],
    ];
  }

  /**
   * Tests the getTermNameByTid() method.
   *
   * @param int|string $term_id
   *   The term ID.
   * @param string $expected_name
   *   The expected term name.
   * @param bool $term_exists
   *   Whether the term exists.
   * @param string|null $term_name
   *   The term name.
   * @param string $language_code
   *   The language code.
   *
   * @dataProvider termNameProvider
   */
  public function testGetTermNameByTid(int|string $term_id, string $expected_name, bool $term_exists, string|null $term_name, string $language_code) {
    // Set up the mocks for term storage and translation based on the
    // data provider input.
    $term_storage_mock = $this->createMock(EntityStorageInterface::class);
    $term = $term_exists ? $this->createConfiguredMock(TermInterface::class, [
      'getName' => $term_name,
      'language' => $this->mockedLanguage,
    ]) : NULL;

    // Adjust language mock based on the provided language code.
    $this->mockedLanguage->method('getId')->willReturn($language_code);

    $term_storage_mock->method('load')->willReturn($term);

    // Mock the entity type manager to return our mocked term storage.
    $this->entityTypeManager->method('getStorage')->willReturn($term_storage_mock);

    // Mock the translation handling based on the existence of the term and
    // the provided language code.
    $this->entityRepository->method('getTranslationFromContext')->willReturn($term);

    // Now call the method under test with the provided term ID.
    $this->assertEquals($expected_name, $this->tidToNameTwigExtension->getTermNameByTid($term_id));
  }

}
