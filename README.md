#Term ID to Name
The tid_to_name module provides a Twig function, tn, that allows you to fetch the name of a taxonomy term based on its Term ID (TID).

##Features
Simple Twig function for fetching term names.
Integrated with Drupal's multilingual system to return translated term names based on the current language.

##Usage
Once the module is installed and enabled, you can use the tn Twig function in your templates like so:

```
{{ tn(123) }} 
```

Where 123 is the TID of the taxonomy term whose name you want to fetch. If the term exists, the function will output the term's name. If the term doesn't exist or the TID is invalid, the function will return an empty string.

This is especially useful in Views when you wish to override a title when using term contextual filters.

##Requirements
- Drupal 9 or 10.
- Taxonomy module (part of Drupal core).
